package com.smartShoppingCart.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.smartShoppingCart.Billing.ProductBill;
import com.smartShoppingCart.Billing.ReadXMLFiles;
import com.smartShoppingCart.ShoppingEntities.Categories;
import com.smartShoppingCart.ShoppingEntities.Category;
import com.smartShoppingCart.ShoppingEntities.FlatDiscountSlabs;
import com.smartShoppingCart.ShoppingEntities.Item;
import com.smartShoppingCart.ShoppingEntities.ShoppingCart;
import com.smartShoppingCart.ShoppingEntities.Slab;

public class ProductBillingTest {
	static File shoppingCartFile = new File(
			"src/main/resources/ShoppingCart.xml");
	static File categoriesFile = new File("src/main/resources/Categories.xml");
	static File flatDiscountSlabsFile = new File(
			"src/main/resources/FlatDiscountSlabs.xml");
	ProductBill productBill = new ProductBill();
	ReadXMLFiles readXMLFiles = new ReadXMLFiles();
	List<Item> scList;
	List<Category> catList;

	public static Object readXmlIntoObject(File fileObj, JAXBContext jc) {
		Object xmlToJavaObj = null;
		try {
			Unmarshaller jaxbUnmarshaller = jc.createUnmarshaller();
			xmlToJavaObj = jaxbUnmarshaller.unmarshal(fileObj);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlToJavaObj;
	}

	@SuppressWarnings("deprecation")
	@Test
	public void assertGrandTotal() throws JAXBException {
		final float grandTotalExpected = 1634.2f;

		JAXBContext scJaxbContext = JAXBContext.newInstance(ShoppingCart.class);
		ShoppingCart sc = (ShoppingCart) readXmlIntoObject(shoppingCartFile,
				scJaxbContext);
		scList = sc.getItemList();
		JAXBContext catJaxbContext = JAXBContext.newInstance(Categories.class);
		Categories cat = (Categories) readXmlIntoObject(categoriesFile,
				catJaxbContext);
		catList = cat.getCategories();
		float grandtotalCalculated = productBill.generateProductBill(scList,
				catList);
		assertEquals(grandTotalExpected, grandtotalCalculated, 0.0);

	}

	@Test
	public void assertNetBillAmount() throws JAXBException {
		final float netBillAmountExpected = 1601.516f;
		JAXBContext scJaxbContext = JAXBContext.newInstance(ShoppingCart.class);
		ShoppingCart sc = (ShoppingCart) readXmlIntoObject(shoppingCartFile,
				scJaxbContext);
		scList = sc.getItemList();
		JAXBContext catJaxbContext = JAXBContext.newInstance(Categories.class);
		Categories cat = (Categories) readXmlIntoObject(categoriesFile,
				catJaxbContext);
		catList = cat.getCategories();
		JAXBContext fdsJaxbContext = JAXBContext
				.newInstance(FlatDiscountSlabs.class);
		FlatDiscountSlabs fds = (FlatDiscountSlabs) readXmlIntoObject(
				flatDiscountSlabsFile, fdsJaxbContext);
		List<Slab> slablist = fds.getSlabs();

		ProductBill pb = new ProductBill();
		float grandtotal = pb.generateProductBill(scList, catList);

		float netBillAmountCalculated = pb
				.grandtotalSlabs(grandtotal, slablist);

		assertEquals(netBillAmountExpected, netBillAmountCalculated, 0.0);
	}
	
	

}
