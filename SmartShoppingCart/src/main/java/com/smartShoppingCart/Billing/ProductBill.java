package com.smartShoppingCart.Billing;

import java.util.List;

import com.smartShoppingCart.ShoppingEntities.Category;
import com.smartShoppingCart.ShoppingEntities.Item;
import com.smartShoppingCart.ShoppingEntities.Slab;

public class ProductBill {
	// For Output 1
	public float generateProductBill(List<Item> scList, List<Category> catList) {
		float totaldiscountedPrice = 0;
		for (Item c : scList) {

			float itemPrice = c.getUnitPrice();
			int itemQuantity = c.getQuantity();
			float purchasePrice = itemPrice * itemQuantity;

			for (Category d : catList) {
				int catId = d.getId();
				float catDiscPer = d.getDiscPerc();
				if (catId == c.getItemCategoryID()) {
					float discountAmount = (purchasePrice * catDiscPer / 100);
					float discountedPrice = purchasePrice - discountAmount;

					totaldiscountedPrice = totaldiscountedPrice
							+ discountedPrice;
					System.out.println("ItemizedBill::\n" + "ItemName: "
							+ c.getItemName() + " Quantity: " + itemQuantity
							+ " Unit Price: " + itemPrice + " Discount%: "
							+ d.getDiscPerc()
							+ " Net Purchase amount of item: "
							+ discountedPrice);
				}

			}
		}

		return totaldiscountedPrice;

	}

	// For output 2
	public float grandtotalSlabs(float grandtotal, List<Slab> slablist) {
		float discountamount = 0;
		float netBillAmount = 0;

		for (Slab s : slablist) {

			if (grandtotal > s.getRangeMin() && grandtotal <= s.getRangeMax()) {

				discountamount = grandtotal * s.getDiscPerc() / 100;

				netBillAmount = grandtotal - discountamount;
			}
		}
		System.out.println("Grandtotal:" + grandtotal);
		System.out.println("Applicable Discount:" + discountamount);
		System.out.println("Net Bill Amount:" + netBillAmount);
		return netBillAmount;
	}
}
