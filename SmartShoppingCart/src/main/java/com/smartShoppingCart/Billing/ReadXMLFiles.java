package com.smartShoppingCart.Billing;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.smartShoppingCart.ShoppingEntities.Categories;
import com.smartShoppingCart.ShoppingEntities.Category;
import com.smartShoppingCart.ShoppingEntities.FlatDiscountSlabs;
import com.smartShoppingCart.ShoppingEntities.Item;
import com.smartShoppingCart.ShoppingEntities.ShoppingCart;
import com.smartShoppingCart.ShoppingEntities.Slab;

public class ReadXMLFiles {
	static File shoppingCartFile = new File("src/main/resources/ShoppingCart.xml");
	static File categoriesFile = new File("src/main/resources/Categories.xml");
	static File flatDiscountSlabsFile = new File("src/main/resources/FlatDiscountSlabs.xml");



	public static void main(String[] args) throws JAXBException {
		generateBill();
		
	}
	
	public static void generateBill() throws JAXBException{
		JAXBContext scJaxbContext = JAXBContext.newInstance(ShoppingCart.class);
		ShoppingCart sc=(ShoppingCart) readXmlIntoObject(shoppingCartFile,scJaxbContext);
		List<Item> scList=sc.getItemList();
		JAXBContext catJaxbContext = JAXBContext.newInstance(Categories.class);
		Categories cat=(Categories) readXmlIntoObject(categoriesFile,catJaxbContext);
		List<Category> catList=cat.getCategories();
		
		JAXBContext fdsJaxbContext = JAXBContext.newInstance(FlatDiscountSlabs.class);
		FlatDiscountSlabs fds=(FlatDiscountSlabs) readXmlIntoObject(flatDiscountSlabsFile,fdsJaxbContext);
		List<Slab> slablist=fds.getSlabs();
		
		
		ProductBill pb= new ProductBill();
		float grandtotal = pb.generateProductBill(scList, catList);
		
		pb.grandtotalSlabs(grandtotal, slablist);
		
	}
	
	public static Object readXmlIntoObject(File fileObj,JAXBContext jc){
		Object xmlToJavaObj = null;
		try {
			Unmarshaller jaxbUnmarshaller = jc.createUnmarshaller();
			xmlToJavaObj =  jaxbUnmarshaller.unmarshal(fileObj);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlToJavaObj;
	}
}
