package com.smartShoppingCart.ShoppingEntities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ShoppingCart")
public class ShoppingCart {
	private List<Item> itemList=new ArrayList<Item>();
	
	
	@XmlElement(name="Item")
	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	
	

}
