package com.smartShoppingCart.ShoppingEntities;

import javax.xml.bind.annotation.XmlElement;

public class Slab {
	private int rangeMin;
	private int rangeMax;
	private int discPerc;
	
	@XmlElement
	public int getRangeMin() {
		return rangeMin;
	}
	public void setRangeMin(int rangeMin) {
		this.rangeMin = rangeMin;
	}
	@XmlElement
	public int getRangeMax() {
		return rangeMax;
	}
	public void setRangeMax(int rangeMax) {
		this.rangeMax = rangeMax;
	}
	@XmlElement
	public int getDiscPerc() {
		return discPerc;
	}
	public void setDiscPerc(int discPerc) {
		this.discPerc = discPerc;
	}
	
	
}
