package com.smartShoppingCart.ShoppingEntities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Categories")
public class Categories {
	private List<Category> categories=new ArrayList<Category>();

	@XmlElement(name="Category")
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
}
