package com.smartShoppingCart.ShoppingEntities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="FlatDiscountSlabs")
public class FlatDiscountSlabs {
private List<Slab> slabs=new ArrayList<Slab>();

@XmlElement(name="Slab")
public List<Slab> getSlabs() {
	return slabs;
}

public void setSlabs(List<Slab> slabs) {
	this.slabs = slabs;
}


}
