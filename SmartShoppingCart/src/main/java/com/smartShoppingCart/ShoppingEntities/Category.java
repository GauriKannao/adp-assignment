package com.smartShoppingCart.ShoppingEntities;

import javax.xml.bind.annotation.XmlElement;


public class Category {
 private int id;
 private String name;
 private int discPerc;
 @XmlElement
 public int getId() {
		return id;
	}
 
 
	public void setId(int id) {
		this.id = id;
	}
	@XmlElement
	public String getName() {
		return name;
	}
	 
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement
	public int getDiscPerc() {
		return discPerc;
	}
	 
	public void setDiscPerc(int discPerc) {
		this.discPerc = discPerc;
	}

}
