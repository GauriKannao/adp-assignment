package com.smartShoppingCart.ShoppingEntities;

import javax.xml.bind.annotation.XmlElement;


public class Item {
	
	private int itemID;
	private int itemCategoryID;
	private String itemName;
	private int unitPrice;
	private int quantity;
	
	@XmlElement
	public int getItemID() {
		return itemID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	@XmlElement
	public int getItemCategoryID() {
		return itemCategoryID;
	}
	public void setItemCategoryID(int itemCategoryID) {
		this.itemCategoryID = itemCategoryID;
	}
	@XmlElement
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@XmlElement
	public int getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}
	@XmlElement
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
